import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';

import 'package:flutter_openvpn/flutter_openvpn.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    FlutterOpenvpn.init(
            localizedDescription: "FlutterOpenVPN",
            providerBundleIdentifier: "com.yun.flutteropenvpn.VPNExtension")
        .then((initStatus) => print(initStatus));
  }

  void connectVPN() async {
    final String _ovpnFileContents =
        await rootBundle.loadString('assets/ott-mobile-sa-vpn-client.ovpn');
    await FlutterOpenvpn.launchVPN(
      _ovpnFileContents,
      (isProfileLoaded) => log('isProfileLoaded $isProfileLoaded'),
      (status) => log('onVPNStatusChanged $status'),
      onConnectionStatusChanged:
          (duration, lastPacketRecieve, byteIn, byteOut) {
        log('onConnectionStatusChanged $duration, $lastPacketRecieve, $byteIn, $byteOut');
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: ElevatedButton(
          child: const Text('Start VPN'),
          onPressed: connectVPN,
        ),
      ),
    );
  }
}
