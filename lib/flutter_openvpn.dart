// ignore_for_file: avoid_print

import 'dart:async';

import 'package:flutter/services.dart';

typedef OnConnectionStatusChanged = Function(
    String duration, String lastPacketRecieve, String byteIn, String byteOut);

/// Track vpn profile status.
typedef OnProfileStatusChanged = Function(bool isProfileLoaded);

/// Track Vpn status.
///
/// Status strings are but not limited to:
/// "CONNECTING", "CONNECTED", "DISCONNECTING", "DISCONNECTED", "INVALID", "REASSERING", "AUTH", ...
/// print status to get full insight.
/// status might change depending on platform.
typedef OnVPNStatusChanged = Function(String status);

const String _profileLoaded = "profileloaded";
const String _profileLoadFailed = "profileloadfailed";
const String _connectionUpdate = 'connectionUpdate';

class FlutterOpenvpn {
  static const MethodChannel _channel = MethodChannel('flutter_openvpn');
  static OnProfileStatusChanged? _onProfileStatusChanged;
  static OnVPNStatusChanged? _onVPNStatusChanged;
  static OnConnectionStatusChanged? _onConnectionStatusChanged;

  /// Initialize plugin.
  ///
  /// Must be called before any use.
  ///
  /// localizedDescription and providerBundleIdentifier is only required on iOS.
  ///
  /// localizedDescription : Name of vpn profile in settings.
  ///
  /// providerBundleIdentifier : Bundle id of your vpn extension.
  ///
  /// returns {"currentStatus" : "VPN_CURRENT_STATUS",
  ///
  /// "expireAt" : "VPN_EXPIRE_DATE_STRING_IN_FORMAT(yyyy-MM-dd HH:mm:ss)",} if successful
  ///
  ///  returns null if failed
  static Future<dynamic> init(
      {String? providerBundleIdentifier, String? localizedDescription}) async {
    dynamic initStatus = await _channel.invokeMethod("init", {
      'localizedDescription': localizedDescription,
      'providerBundleIdentifier': providerBundleIdentifier,
    }).catchError((error) => error);
    if (initStatus is! PlatformException) {
      _channel.setMethodCallHandler((call) {
        switch (call.method) {
          case _connectionUpdate:
            _onConnectionStatusChanged?.call(
                call.arguments['duration'],
                call.arguments['lastPacketRecieve'],
                call.arguments['byteIn'],
                call.arguments['byteOut']);
            break;
          case _profileLoaded:
            _onProfileStatusChanged?.call(true);
            break;
          case _profileLoadFailed:
            _onProfileStatusChanged?.call(false);
            break;
          default:
            _onVPNStatusChanged?.call(call.method);
        }
        return initStatus;
      });
      return initStatus;
    } else {
      print('flutter_openvpn Initilization failed');
      print('flutter_openvpn ${initStatus.message}');
      print('flutter_openvpn ${initStatus.details}');
      return null;
    }
  }

  /// Load profile and start connecting.
  ///
  /// if expireAt is provided
  /// Vpn session stops itself at given date.
  static Future<int?> launchVPN(
    String ovpnFileContents,
    OnProfileStatusChanged? onProfileStatusChanged,
    OnVPNStatusChanged? onVPNStatusChanged, {
    String? user,
    String? pass,
    OnConnectionStatusChanged? onConnectionStatusChanged,
  }) async {
    _onProfileStatusChanged = onProfileStatusChanged;
    _onVPNStatusChanged = onVPNStatusChanged;
    _onConnectionStatusChanged = onConnectionStatusChanged;

    dynamic launchStatus = await _channel.invokeMethod(
      "launch",
      {
        'ovpnFileContent': ovpnFileContents,
        'user': user,
        'pass': pass,
      },
    ).catchError((error) => error);

    if (launchStatus == null) return 0;
    print((launchStatus as PlatformException).message);
    return int.tryParse(launchStatus.code);
  }

  /// stops any connected session.
  static Future<void> stopVPN() async {
    await _channel.invokeMethod("stop");
  }
}
